package com.example.encryptdecrypt;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.encryptdecrypt.crypto.AsymmetricCipher;
import com.example.encryptdecrypt.utils.Alert;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextUser, edtMessage;
    private RadioButton rdbEncrypt, rdbDecrypt;
    private FloatingActionButton fab;
    private AsymmetricCipher cipher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextUser = (EditText) findViewById(R.id.edtUser);
        edtMessage = (EditText) findViewById(R.id.edtMessage);
        rdbEncrypt = (RadioButton) findViewById(R.id.rdbEncrypt);
        rdbDecrypt = (RadioButton) findViewById(R.id.rdbDecrypt);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        Alert.setContext(getApplicationContext(), toolbar);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Alert.setContext(getApplicationContext(), v);
        if(rdbEncrypt.isChecked()) {
            encryptMessage();
        } else if(rdbDecrypt.isChecked()) {
            decryptMessage();
        }
    }

    public void doOperation(View view) {
        if(rdbEncrypt.isChecked()) {
            encryptMessage();
        } else if(rdbDecrypt.isChecked()) {
            decryptMessage();
        }
    }

    private void encryptMessage() {
        String text = cipher.encryptFor(edtMessage.getText().toString(), editTextUser.getText().toString());

        edtMessage.setText(text);
        edtMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        edtMessage.setTextColor(getResources().getColor(R.color.primary_dark, null));
    }

    private void decryptMessage() {
        String text = cipher.decryptFor(edtMessage.getText().toString(), editTextUser.getText().toString());
        edtMessage.setText(text);
        edtMessage.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        edtMessage.setTextColor(getResources().getColor(R.color.secondary, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_genkey) {
            cipher = new AsymmetricCipher(getApplicationContext(), editTextUser.getText().toString());
            cipher.generateNewKeys();
        } else if(id == R.id.action_exit) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
