package com.example.encryptdecrypt.crypto;

import android.animation.PropertyValuesHolder;
import android.os.Environment;

import com.example.encryptdecrypt.utils.Alert;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

public class RSAKeyManager {

    private File keyRing;
    private String name;

    public RSAKeyManager (File keyRing, String name) {
        this.keyRing = keyRing;
        this.name = name;
    }

    public static boolean isExternalStorageReady() {
        String extStorageState = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(extStorageState) && !Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public void saveKeys(KeyPair keys) {
        if(!isExternalStorageReady()) {
            Alert.showSnackbar("Not able to write keys");
            return;
        }
        ObjectOutputStream oosPKey = null;
        ObjectOutputStream oosKey = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            //Llave privada
            RSAPrivateKeySpec pkey = keyFactory.getKeySpec(keys.getPrivate(), RSAPrivateKeySpec.class);
            File pkeyFile = new File(keyRing, new String( name + ".pkey"));
            oosPKey = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(pkeyFile)));

            oosPKey.writeObject(pkey.getModulus());
            oosPKey.writeObject(pkey.getPrivateExponent());
            Alert.showToast("Private key saved");
            oosPKey.close();

            //Llave publica
            RSAPublicKeySpec key = keyFactory.getKeySpec(keys.getPublic(), RSAPublicKeySpec.class);
            File keyFile = new File(keyRing, new String(name + ".key"));
            oosPKey = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(keyFile)));

            oosPKey.writeObject(key.getModulus());
            oosPKey.writeObject(key.getPublicExponent());
            Alert.showToast("Public key saved");


        } catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("Invalid Algorithm");
        } catch (FileNotFoundException e) {
            Alert.showSnackbar("File not found");
        } catch (IOException e) {
            Alert.showSnackbar("Error writing");
        } catch (InvalidKeySpecException e) {
            Alert.showSnackbar("Invalid key exception");
            e.printStackTrace();
        }
    }

    public PublicKey readKey(String name) {
        if (!isExternalStorageReady()) {
            Alert.showSnackbar("Not able to read key");
            return null;
        }
        ObjectInputStream oisKey = null;
        try {
            //Llave publica
            File keyFile = new File(keyRing, new String(name + ".key"));
            oisKey = new ObjectInputStream(new BufferedInputStream(new FileInputStream(keyFile)));

            KeyFactory factory = KeyFactory.getInstance("RSA");
            BigInteger mod = (BigInteger) oisKey.readObject();
            BigInteger exp = (BigInteger) oisKey.readObject();
            oisKey.close();

            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(mod, exp);
            PublicKey publicKey = factory.generatePublic(keySpec);
            return publicKey;
        } catch (InvalidKeySpecException e) {
            Alert.showSnackbar("Invalid public key");
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("Invalid Algorithm");
        } catch (FileNotFoundException e) {
            Alert.showSnackbar("No such file found");
        } catch (IOException e) {
            Alert.showSnackbar("Error IO");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PrivateKey readPKey(String name) {
        if(!isExternalStorageReady()) {
            Alert.showSnackbar("No able to read key");
            return null;
        }
        ObjectInputStream oisPKey = null;
        try {
            File keyFIle = new File(keyRing, new String(name + ".pkey"));
            oisPKey = new ObjectInputStream(new BufferedInputStream( new FileInputStream(keyFIle)));

            KeyFactory factory = KeyFactory.getInstance("RSA");
            BigInteger mod = (BigInteger) oisPKey.readObject();
            BigInteger exp = (BigInteger) oisPKey.readObject();
            oisPKey.close();

            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(mod, exp);
            PrivateKey privateKey = factory.generatePrivate(keySpec);
            return privateKey;
        } catch (InvalidKeySpecException e) {
            Alert.showSnackbar("Invalid private key");
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("No such Algorithm");
        } catch (ClassNotFoundException e) {
            Alert.showSnackbar("Invalid class");
        } catch (FileNotFoundException e) {
            Alert.showSnackbar("File not found");
        } catch (IOException e) {
            Alert.showSnackbar("Error IO");
            e.printStackTrace();
        }
        return null;
    }
}
