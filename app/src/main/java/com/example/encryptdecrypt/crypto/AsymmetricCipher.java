package com.example.encryptdecrypt.crypto;

import android.content.Context;

import com.example.encryptdecrypt.utils.Alert;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAKey;
import java.security.spec.RSAPrivateKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymmetricCipher {

    private boolean keysReady;
    private RSAKeyManager keyManager;

    public AsymmetricCipher(Context context, String user) {
        this.keysReady = false;
        this.keyManager = new RSAKeyManager(context.getExternalFilesDir("key_ring"), user);
    }

    public void generateNewKeys() {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(1024);
            KeyPair keyPair = keyPairGenerator.genKeyPair();

            keyManager.saveKeys(keyPair);
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("Unknow algorithm");
        }
        keysReady = true;
        Alert.showToast("Keys ready");
    }

    public String encryptFor(String plainText, String user) {
        if(!keysReady) {
            return "No keys available";
        }
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            PublicKey publicKey = keyManager.readKey(user);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte [] c = cipher.doFinal(plainText.getBytes("UTF-8"));
            String cipherText = Base64.getEncoder().encodeToString(c);
            Alert.showToast("Text Encrypted");
            return cipherText;
        } catch (BadPaddingException e) {
            Alert.showSnackbar("Error: Bad Padding");
        } catch (IllegalBlockSizeException e) {
            Alert.showSnackbar("Illegal block size");
        } catch (InvalidKeyException e) {
            Alert.showSnackbar("Invalid Key");
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("Unknown Algorithm");
        } catch (NoSuchPaddingException e) {
            Alert.showSnackbar("Bad Padding");
        } catch (UnsupportedEncodingException e) {
            Alert.showSnackbar("Unsupported Encoding Exception");
        }
        return null;
    }

    public String decryptFor(String cipherText, String user) {
        if(!keysReady) {
            return "No keys available";
        }
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            PrivateKey privateKey = keyManager.readPKey(user);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte [] p = Base64.getDecoder().decode(cipherText);
            String plainText = new String(cipher.doFinal(p));
            Alert.showToast("Text Decrypted");
            return plainText;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }
}
