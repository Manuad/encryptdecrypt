package com.example.encryptdecrypt.crypto;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import com.example.encryptdecrypt.utils.Alert;

public class SymetricCipher {
    private SecretKeySpec secretKey;
    private final String algorithm;
    private final String cipherMode;
    private final String padding;

    public SymetricCipher(String algorithm, String cipherMode, String padding) {
        this.algorithm = algorithm;
        this.cipherMode = cipherMode;
        this.padding = padding;
    }

    public void setKey(String password) {
        try {
            byte[] passKey = password.getBytes(StandardCharsets.UTF_8);
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            passKey = md.digest(passKey);
            passKey = Arrays.copyOf(passKey, 16);
            secretKey = new SecretKeySpec(passKey, algorithm);
        } catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("Error: Invalid algorithm");
        } catch (UnsupportedOperationException e) {
            Alert.showSnackbar("Error: Unsopported encoding format");
        }
        Alert.showToast("Key Saved");
    }

    public String encrypt(String plainText) {
        String cipherScheme = algorithm + "/" + cipherMode + "/" + padding;
        try {
            Cipher cipher = Cipher.getInstance(cipherScheme);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte [] c = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
            String cipherText = Base64.getEncoder().encodeToString(c);
            Alert.showToast("Text encrypted");
            return cipherText;
        }
        catch (NoSuchAlgorithmException e) {
            Alert.showSnackbar("Error: Invalid encryption algorithm");
        }
        catch (InvalidKeyException e) {
            Alert.showSnackbar("Error: Invalid key format");
        }
        catch (NoSuchPaddingException e) {
            Alert.showToast("Error: Invalid padding algorithm");
        }
        catch (BadPaddingException e) {
            Alert.showSnackbar("Error: Bad Padding");
        }
        catch (UnsupportedOperationException e) {
            Alert.showToast("Error: Unsupported encoding format");
        }
        catch (IllegalBlockSizeException e) {
            Alert.showSnackbar("Error: Illegal block size");
        }
        return null;
    }

    public String decrypt(String cipherText) {
        String cipherScheme = algorithm + "/" + cipherMode + "/" + padding;
        try {
            Cipher cipher = Cipher.getInstance(cipherScheme);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte [] p = Base64.getDecoder().decode(cipherText);
            String plainText = new String(cipher.doFinal(p));
            Alert.showToast("Text decrypted");
            return plainText;
        } catch (NoSuchAlgorithmException e) {
            Alert.showToast("Error: Invalid encryption message");
        } catch (InvalidKeyException e) {
            Alert.showSnackbar("Error: Invalid key");
        } catch (NoSuchPaddingException e) {
            Alert.showToast("Error: Invalid padding algorithm");
        } catch (BadPaddingException e) {
            Alert.showSnackbar("Error: Bad padding");
        } catch (IllegalBlockSizeException e) {
            Alert.showSnackbar("Error: Illegal block size");
        }
        return null;
    }
}
